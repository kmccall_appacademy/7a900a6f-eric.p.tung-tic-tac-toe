class ComputerPlayer
  attr_accessor :name, :mark
  attr_reader :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    available_positions = []
    (0..2).each do |row|
      (0..2).each do |col|
        available_positions << [row,col] if board.empty?([row,col])
      end
    end
    available_positions.each { |position| return position if winning_move?(position) }
    available_positions.sample
  end

  def winning_move?(position)
    board.place_mark(position, mark)
    (board.winner == mark ? result = true : result = false)
    board.remove_mark(position)
    result
  end
end
