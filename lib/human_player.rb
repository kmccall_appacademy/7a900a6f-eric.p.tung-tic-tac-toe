class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    temp = Array.new(3) {Array.new(3) {" "}}
    board.grid.each_with_index do |row_marks, row_idx|
      row_marks.each_with_index do |col_marks, col_idx|
        temp[row_idx][col_idx] = col_marks unless col_marks.nil?
      end
    end
    puts "   0   1   2 "
    puts "0: #{temp[0][0]} | #{temp[0][1]} | #{temp[0][2]} "
    puts "  -----------"
    puts "1: #{temp[1][0]} | #{temp[1][1]} | #{temp[1][2]} "
    puts "  -----------"
    puts "2: #{temp[2][0]} | #{temp[2][1]} | #{temp[2][2]} "
  end

  def get_move
    puts "#{name}, where would you like to move?"
    player_response = gets.chomp.split(",")
    row = player_response.first.to_i
    col = player_response.last.to_i
    [row,col]
  end

end
