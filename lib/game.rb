require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @board = Board.new
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @player_one.mark = :X
    @player_two.mark = :O
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play_turn
    @current_player.display(@board)
    position = @current_player.get_move
    @board.place_mark(position, @current_player.mark)
    switch_players!
  end

  def play
    until self.board.over?
      play_turn
    end
    switch_players!
    if self.board.winner.nil?
      puts "There is no winner :("
    else
      puts "Congratulations #{@current_player.name}, you win!"
    end
  end

end
