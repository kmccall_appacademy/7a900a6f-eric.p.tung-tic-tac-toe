class Board
  attr_accessor :grid
  MARKS = [:X,:O]

  def initialize(grid = nil)
    if grid.nil?
      @grid = Array.new(3) {Array.new(3) }
    else
      @grid = grid
    end
  end

  def place_mark(position, mark)
    @grid[position.first][position.last] = mark if empty?(position)
  end

  def remove_mark(position)
    @grid[position.first][position.last] = nil
  end

  def empty?(position)
    grid[position.first][position.last].nil?
  end

  def winner
    MARKS.each { |mark| return mark if won?(mark) }
    nil
  end

  def over?
    # all slots on grid filled?
    if full_board?
      return true
    else
      return true unless winner.nil?
    end
    false
  end

  def full_board?
    return false if grid.any? { |row| row.any? { |x| x.nil? }}
    true
  end

  def won?(mark)
    matches_mark=[]
    (0..2).each do |row|
      (0..2).each do |col|
        unless grid[row][col].nil?
          matches_mark << [row, col] if grid[row][col]==mark
        end
      end
    end
    return false if matches_mark.length < 3
    combos = matches_mark.combination(3).to_a
    # all have same first
    combos.each do |combo|
      return true if combo.all? { |x| x.first == combo[0].first }
      # all have same last
      return true if combo.all? { |x| x.last == combo[0].last }
      # diagonals [[0,0],[1,1],[2,2]]
      return true if combo.all? { |x| x.first == x.last }
      # diagonals [[0,2],[1,1],[2,0]]
      return true if combo.all? { |x| [[0,2],[1,1],[2,0]].include?(x) }
    end
    false
  end

end
